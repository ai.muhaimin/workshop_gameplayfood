using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : MonoBehaviour
{

    public float speed = 10;
    public float delayDestoy;

    public float topBound = -10;
    public bool isAnimal;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        if (isAnimal)
        {
            if(transform.position.z < topBound)
            {
                Destroy(this.gameObject);
            }
        }
    }


    public void DestoryObj()
    {
        StartCoroutine(delayDestroyObj());
    }
    IEnumerator delayDestroyObj()
    {
        yield return new WaitForSeconds(delayDestoy);
        Destroy(this.gameObject);
    }
}
