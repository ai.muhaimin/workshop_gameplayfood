using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodTrigger : MonoBehaviour
{

    public GameManager Manager;
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject.name);
        if(other.gameObject.tag == "animal")
        {
            Debug.Log("animal from food");
            Manager.AddPoint();
            Destroy(this.gameObject);
            Destroy(other.gameObject);
        }
    }
}
