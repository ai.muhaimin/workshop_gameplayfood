using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameManager GameManager;
    public float HorizontalInput;
    public float speed = 10.0f;
    public float xRange = 10;

    // prefab food
    public GameObject projectilePrefab;
    // Update is called once per frame
    void Update()
    {
       
        
        HorizontalInput = Input.GetAxis("Horizontal");
        transform.Translate(Vector3.right * HorizontalInput * Time.deltaTime * speed);

        if (transform.position.x < -xRange)
        {
            transform.position = new Vector3(-xRange, transform.position.y, transform.position.z);
        }

        if (transform.position.x > xRange)
        {
            transform.position = new Vector3(xRange, transform.position.y, transform.position.z);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            // instantiate food
            GameObject foodInstantiate = Instantiate(projectilePrefab, transform.position, projectilePrefab.transform.rotation);
            MoveForward foodForward = foodInstantiate.GetComponent<MoveForward>();
            FoodTrigger foodTrigger = foodInstantiate.GetComponent<FoodTrigger>();

            foodTrigger.Manager = GameManager;
            foodForward.DestoryObj();
        }

    }
}
