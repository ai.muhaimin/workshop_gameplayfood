using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class PlayerTrigger : MonoBehaviour
{

    public GameManager GameManager;
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject.name);
        if (other.gameObject.tag == "animal")
        {
            //Debug.Log("kena animal");

            Destroy(other.gameObject);
            // ngurangin nyawa
            GameManager.ReduceLive();
        }
    }
}
