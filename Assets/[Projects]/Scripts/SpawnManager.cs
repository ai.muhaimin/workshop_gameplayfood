using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    [SerializeField] private GameObject[] animalPrefab;
    [SerializeField] int animalIndex;
    [SerializeField] float spawnRangeX = 15;

    [SerializeField] float startDelay = 2;
    [SerializeField] float Interval = 2;

    // Update is called once per frame
    private void Start()
    {
        InvokeRepeating("SpawnAnimalRandom", startDelay, Interval);
    }

    public void stopSpawn()
    {
        CancelInvoke();
    }

    public void SpawnAnimalRandom()
    {
        //Debug.Log("spawn animal");
        int randomindex = UnityEngine.Random.Range(0, animalPrefab.Length);
        animalIndex = randomindex;
        //Debug.Log(animalIndex);

        Vector3 spawnPos = new Vector3(UnityEngine.Random.Range(-spawnRangeX, spawnRangeX), 0, UnityEngine.Random.Range(20, 30));
        GameObject animal = Instantiate(animalPrefab[animalIndex], spawnPos, animalPrefab[animalIndex].transform.rotation);
    }

}
