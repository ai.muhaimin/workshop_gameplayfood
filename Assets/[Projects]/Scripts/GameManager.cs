using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    [Header("Live")]
    [SerializeField] int CurrentLivePlayer;
    [SerializeField] int MaxLivePlayer;
    [SerializeField] TMP_Text LiveText;


    [Header("Point")]
    [SerializeField] int CurrentPointPlayer;
    [SerializeField] TMP_Text PointText;


    [Header("SpawnManager")]
    [SerializeField] SpawnManager SpawnManager;


    [Header("Game End")]
    [SerializeField] GameObject GameOverView;

    private void Start()
    {
        MaxLivePlayer = 3;
        CurrentLivePlayer = MaxLivePlayer;
        LiveText.text = CurrentLivePlayer.ToString();



        CurrentPointPlayer = 0;
        PointText.text = CurrentPointPlayer.ToString();
    }

    public void ReduceLive()
    {
        CurrentLivePlayer = CurrentLivePlayer - 1;

        if (CurrentLivePlayer > 1)
        {
            LiveText.color = Color.black;
        }

        if (CurrentLivePlayer <= 1)
        {
            LiveText.color = Color.red;
        }


        if(CurrentLivePlayer <= 0)
        {
            CurrentLivePlayer = 0;

            //game over
            Debug.Log("game over");
            SpawnManager.stopSpawn();
            GameOverView.SetActive(true);
        }

        LiveText.text = CurrentLivePlayer.ToString();

    }



    public void AddPoint()
    {
        CurrentPointPlayer = CurrentPointPlayer + 1;
        PointText.text = CurrentPointPlayer.ToString();
    }


    public void PlayAgain()
    {
        SceneManager.LoadScene("gameplay");
    }
}
